#include "stdio.h"
#include "stdlib.h"
#define stringlen 30
#include "stringio.h"

typedef struct DB{
  int roll;
  char *name;
  char *sex;
} S;

int main() {
  S a[10]; int i, *array, len = 2; char f[30] = { 'f', 'e', 'm', 'a', 'l', 'e' };

  for(i = 0; i < len; i++) {
    printf("Enter name %d: ", i + 1);
    a[i].name = getstring();
    printf("Enter roll %d: ", i + 1);
    scanf("%d", &a[i].roll); getchar();
    printf("Enter sex (Female/Male) %d: ", i + 1);
    a[i].sex = getstring();
    printf("\n");
  }

  for(i = 0; i < len; i++) {
    if(comparestring(a[i].sex, f)) {
      a[i].name = capitalstring(a[i].name);
    } else {
      a[i].name = camelstring(a[i].name);
    }
  }

  for(i = 0; i < len; i++) {
    printf("Name: "); putstring(a[i].name);
    printf(", Roll: %d", a[i].roll);
    printf(", Sex: "); putstring(a[i].sex);
    printf("\n");
  }

  return 1;
}