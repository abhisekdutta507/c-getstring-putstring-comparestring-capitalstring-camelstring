char *getstring() {
  char ch, *s; int i = 0;
  s = (char *)malloc(stringlen * sizeof(char));
  do {
    scanf("%c", &ch);
    s[i++] = ch;
  } while(ch != '\n' && i < stringlen);
  
  return s;
}

void putstring(char *s) {
  int i = 0;
  while(s[i] != '\n' && i < stringlen) {
    printf("%c", s[i++]);
  }
}

int comparestring(char *s1, char *s2) {
  int i = 0, k1, k2;
  while(s1[i] != '\n' && s2[i] != '\n' && i < stringlen) {
    k1 = s1[i]; k2 = s2[i];
    if(k1 != k2 && k2 - 32 != k1) { return 0; }
    i++;
  }
  return 1;
}

char *capitalstring(char *s) {
  int i = 0, k = 0;
  while(s[i] != '\n' && i < stringlen) {
    k = s[i];
    if(k >= 97 && k <= 122) { k = k - 32; s[i] = k; }
    i++;
  }
  return s;
}

char *camelstring(char *s) {
  int i = 0, k = 0, change = 1;
  while(s[i] != '\n' && i < stringlen) {
    k = s[i];
    if(change && (k >= 97 && k <= 122)) { k = k - 32; s[i] = k; change = 0; }
    else if(!change && (k >= 65 && k <= 90)) { k = k + 32; s[i] = k; }
    else if(k == 32) { change = 1; }
    else if((k >= 97 && k <= 122) || (k >= 65 && k <= 90)) { change = 0; }
    i++;
  }
  return s;
}